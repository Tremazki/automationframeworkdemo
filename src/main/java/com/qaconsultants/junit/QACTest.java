package com.qaconsultants.junit;

import com.qaconsultants.App;
import com.qaconsultants.config.Config;
import com.qaconsultants.config.TOMLParser;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import java.io.File;

import static com.qaconsultants.App.setSystemInformation;

/**
 * QACTest is a base-class for our Unit-Tests to be able to setup and read a configuration file prior to execution.
 * This allows our tests to be ran through an IDE without requiring setup through the main method.
 */
public class QACTest {

    /**
     * STATIC methods annotated with the @BeforeClass annotation will be run prior to each test CLASS.
     * This is run only once for this QACTest, for example.
     * <p>
     * If we had any content that needed to be loaded, or context that needed to be set for every @QACTest within,
     * this is where we would do so.
     */
    @BeforeClass
    public static void beforeClass() throws Exception {
        App.setupConfiguration(new String[0]);
    }

    /**
     * STATIC methods annotated with the @AfterClass annotation will be run after each test CLASS.
     * This is run only once for this QACTest, for example.
     * <p>
     * If we had any content that needed to be unloaded, this is where we would do so.
     */
    @AfterClass
    public static void afterClass() {
    }
}

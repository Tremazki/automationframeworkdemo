package com.qaconsultants.config;

import com.qaconsultants.config.model.Section;
import com.qaconsultants.utils.ReflUtils;
import org.apache.commons.cli.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * Centralized location of all config information.
 *
 * Appending a new TOML Section:
 *  - Create a new internal public class extending from Section (optional extension, but recommended)
 *  - Create a public variable within that class for each configuration option
 *  - Add an instance of this new internal class to Config and instantiate it in the constructor.
 *
 *  For example:
 *
 *  Example.toml
 *  ------------
 *  [mySection]
 *  test="test"
 *  test2="test2"
 *
 *
 *  Config.java
 *  -----------
 *  public class Config
 *  {
 *      public class mySection extends Section
 *      {
 *          public String test;
 *          public String test2;
 *      }
 *      public mySection section;
 *      ...
 *  }
 *
 *  With this setup, the TOML parser will automatically assign the variables with their appropriate values
 *  from the external configuration file.
 *
 */
public class Config
{
    //Contains all data under the [project] header in the TOML file
    public class Project extends Section {
        public String name = "Default Project";
        public boolean generateReport = false;
    }

    //Contains all data under the [pathing] header in the TOML file
    public class Pathing extends Section {
        public String webdrivers = "./resources/webdrivers/";
        public String output     = "./output/";
    }

    public class Selenium extends Section {
        public int timeout = 15;
    }

    public Project                    project;
    public Pathing                    pathing;
    public Selenium                   selenium;
    public Map<String, Object>        capabilities;

    /**
     *
     * Constructor
     *
     * Initialize all sections to their defaults values
     *
     */
    public Config() {
        project      = new Project();
        pathing      = new Pathing();
        selenium     = new Selenium();
        capabilities = new HashMap<>();
    }

    public Config(Parser<Config> _parser, File _file) throws FileNotFoundException, ParseException {
        Config parsed =  _parser.parse(_file);

        this.project      = parsed.project;
        this.pathing      = parsed.pathing;
        this.selenium     = parsed.selenium;
        this.capabilities = parsed.capabilities;
    }

    /**
     *
     * Loop over the fields within Config and determine whether or not they contain a given field.
     * If they do, assign them the supplied parameter [_value]
     *
     * @param _field The field to search for in the section
     * @param _value The value to associate with the field if found
     *
     */
    public void setSectionField(String _field, Object _value) {
        try {
            Section _section;
            Field[] _sections = this.getClass().getFields();
            for (Field field : _sections) {
                _section = (Section) field.get(this);
                if (_section.set(_field, _value))
                    break;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * Directly attempt to access a Config Field with the name supplied by parameter [_section]
     * If we find the section, attempt to find and set a field to the supplied value.
     *
     * @param _section The section to find in this class
     * @param _field   The field to search for in the section
     * @param _value   The value to associate with the field if found
     *
     */
    public void setSectionField(String _section, String _field, Object _value) {
        if (ReflUtils.doesObjectContainField(this, _section)) {
            try {
                Field   sect    = ReflUtils.getObjectField(this, _section);
                Section section = (Section) sect.get(this);
                section.set(_field, _value);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}

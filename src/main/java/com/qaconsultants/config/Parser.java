package com.qaconsultants.config;


import org.apache.commons.cli.ParseException;

import java.io.File;
import java.io.FileNotFoundException;

/**
 *
 * Abstract class for all concrete configuration parser objects
 *
 * @param <T>
 *
 */
public abstract class Parser<T> {

    /**
     *
     * Package private abstract method to prevent end-user from creating Config objects outside of the
     * singleton provided.
     *
     * @return T
     * @throws Exception if parsing fails 
     *
     */
    abstract T parse(File file) throws ParseException, FileNotFoundException;
}

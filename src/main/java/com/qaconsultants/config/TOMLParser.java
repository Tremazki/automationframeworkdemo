package com.qaconsultants.config;

import com.moandjiezana.toml.Toml;

import java.io.File;
import java.io.FileNotFoundException;

/**
 *
 * TOMLParser is a concrete implementation of Parser
 *
 * Utilizes Toml4J to parse the supplied file into its java class representation
 *
 */
public class TOMLParser extends Parser<Config> {

    @Override
    Config parse(File _file) throws FileNotFoundException {
        if (_file.isFile()) {
            return new Toml().read(_file).to(Config.class);
        } else {
            throw new FileNotFoundException("The supplied File does not exist or the path is incorrect");
        }
    }

}

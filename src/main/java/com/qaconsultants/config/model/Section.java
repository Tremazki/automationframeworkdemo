package com.qaconsultants.config.model;

import com.qaconsultants.utils.ReflUtils;

import java.lang.reflect.Field;

/**
 * Optional base-class for Sections in the config model
 */
public abstract class Section
{
    /**
     * Helper function that is available to all sections ease the process of setting the values of their variables.
     *
     * Checks to see whether or not a field exists with the key.
     * If a key is found, the value is set to the value of the method parameter [_value]
     *
     * @param _key   The field in this object to locate
     * @param _value The value to set the field to
     * @return       True if successfully set, False otherwise
     */
    public boolean set(String _key, Object _value)
    {
        Field _field;
        try
        {
            if(!_key.isEmpty() && _value != null)
            {
                if(ReflUtils.doesObjectContainField(this, _key))
                {
                    _field = this.getClass().getDeclaredField(_key);
                    _field.setAccessible(true);
                    _field.set(this, _value);
                    return true;
                }
                else
                    return false;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return false;
    }
}

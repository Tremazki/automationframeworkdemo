package com.qaconsultants.keywords.interaction;

import com.qaconsultants.keywords.StandardKeyword;
import org.openqa.selenium.WebElement;

/**
 *
 * Concrete implementation of Keyword responsible for clicking on the given WebElement
 * using Selenium's default click functionality.
 *
 */
public class Click extends StandardKeyword<WebElement> {
    @Override
    public boolean Execute(WebElement _element, String... _arguments) {
        try {
            _element.click();
            return true;
        }
        catch (Exception e) {
            mLogger.error("An error has occurred in the Click keywords", e);
            throw e;
        }
    }
}

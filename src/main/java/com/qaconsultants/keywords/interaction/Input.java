package com.qaconsultants.keywords.interaction;

import com.qaconsultants.keywords.StandardKeyword;
import org.openqa.selenium.WebElement;

/**
 *
 * Concrete implementation of Keyword responsible for inputting text into the given WebElement
 * using Selenium's default sendKeys functionality.
 *
 */
public class Input extends StandardKeyword<WebElement> {
    private boolean mAppend;

    public Input(){}

    public Input(boolean _append){
        mAppend = _append;
    }

    @Override
    public boolean Execute(WebElement _element, String... _arguments) {
        try {

            if(!mAppend) {
                _element.clear();
            }

            if (_arguments[0] != null) {
                _element.sendKeys(_arguments[0]);
                return true;
            }

            mLogger.warn("No text supplied to input - could not input text");
            return false;
        }
        catch (Exception e) {
            mLogger.error("An error has occurred in the Input keyword", e);
            throw e;
        }
    }
}

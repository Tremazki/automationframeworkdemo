package com.qaconsultants.keywords.interaction;

import com.qaconsultants.keywords.Keyword;
import com.qaconsultants.keywords.StandardKeyword;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

/**
 *
 * Concrete implementation of Keyword responsible for selecting a dropdown option on the given WebElement
 * using Selenium's Select object.
 *
 */
public class Dropdown extends StandardKeyword<WebElement> {

    public enum SelectBy {
        INDEX, VALUE, VISIBLE_TEXT
    }

    // Selenium Select object
    private Select   mSelect;
    // Selection Strategy
    private SelectBy mStrategy;

    /**
     * Constructor
     *
     * @param _strategy Enum value determining how to select from the dropdown
     */
    public Dropdown(SelectBy _strategy) {
        mStrategy = _strategy;
    }

    /**
     * Alternative Constructor
     * <p>
     * Uses a String to determine what SelectBy enum object to use
     *
     * @param _strategy String value to locate a matching enum with
     */
    public Dropdown(String _strategy) {
        mStrategy = SelectBy.valueOf(_strategy);
    }

    @Override
    public boolean Execute(WebElement _element, String... _arguments) {
        try {
            mSelect = new Select(_element);
            switch (mStrategy) {
                case INDEX:
                    selectByIndex(mSelect, Integer.parseInt(_arguments[0]));
                    break;
                case VALUE:
                    selectByValue(mSelect, _arguments[0]);
                    break;
                case VISIBLE_TEXT:
                    selectByText(mSelect, _arguments[0]);
                    break;
                default:
                    return false;
            }
            return true;
        }
        catch (Exception e) {
            mLogger.error("An error has occurred in the Dropdown keyword", e);
            throw e;
        }
    }

    public boolean Execute(WebElement _element, int _index) {
        try {
            mSelect = new Select(_element);
            selectByIndex(mSelect, _index);
            return true;
        } catch (Exception e) {
            mLogger.error("An error has occurred in the Dropdown keyword when attempting to select by index", e);
            throw e;
        }
    }

    /**
     * Select from the drop-down object using an index value.
     *
     * @param _select Selenium Select object
     */
    private void selectByIndex(Select _select, int _index) {
        _select.selectByIndex(_index);
    }

    /**
     * Select from the drop-down object using a value.
     *
     * @param _select Selenium Select object
     */
    private void selectByValue(Select _select, String _value) {
        _select.selectByValue(_value);
    }

    /**
     * Select from the drop-down object using visible text.
     *
     * @param _select Selenium Select object
     */
    private void selectByText(Select _select, String _text) {
        _select.selectByVisibleText(_text);
    }
}

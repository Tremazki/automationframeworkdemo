package com.qaconsultants.keywords;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * Keyword is an abstract class used as a base for all Keywords.
 *
 * The intention behind wrapping Selenium functionality in keywords is for future integration with a keyword
 * driven environment. A client may require tests be run through an external data format, eg: Excel XLSX files.
 *
 * Using this abstraction, we can easily call the functionality that we need in a single line without having to
 * rewrite boilerplate functionality for the new input method.
 *
 * It is recommended that additional reusable Selenium functionality be implemented in Keywords for this purpose.
 *
 * Note:
 *  Templates are used to address the differences in arguments that Keywords may require.
 *  Typically this only changes between WebElement and WebDriver
 *
 * @param <T>
 *
 */
public abstract class Keyword<T, R> {
    protected final static Logger mLogger = LogManager.getLogger(Keyword.class);

    /**
     * Executes the Keyword
     *
     * @param _generic   Generic object to use by the keywords
     * @param _arguments Array of arguments to be used by this Keyword
     */
    public abstract boolean Execute(T _generic, R _arguments) throws Exception;
}

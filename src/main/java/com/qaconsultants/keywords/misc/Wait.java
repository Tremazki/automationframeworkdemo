package com.qaconsultants.keywords.misc;

import com.qaconsultants.keywords.StandardKeyword;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;


/**
 *
 * Concrete implementation of Keyword responsible for waiting
 * until an element is one of the following:
 *
 */
public class Wait extends StandardKeyword<Function<WebDriver, Boolean>> {
    protected WebDriverWait mWait;

    /**
     * Constructor
     *
     * @param _driver WebDriver instance
     */
    public Wait(WebDriver _driver) {
        mWait = new WebDriverWait(_driver, 20);
    }

    @Override
    public boolean Execute(Function<WebDriver, Boolean> _strategy, String... _arguments) throws Exception {
        try {
           mWait.until(_strategy);
           return true;
        }
        catch (Exception e) {
            mLogger.error("An error has occurred in the Wait keyword", e);
            throw e;
        }
    }

    /**
     * Waits for the given Element to be enabled
     *
     * @param _element WebElement
     * @return True if element is enabled, false otherwise
     */
    public static Function<WebDriver, Boolean> waitForEnabledStrategy(WebElement _element){
        return (WebDriver d) -> _element.isEnabled();
    }

    /**
     * Waits for the given Element to be selected
     *
     * @param _element WebElement
     * @return True if element is enabled, false otherwise
     */
    public static Function<WebDriver, Boolean> waitForSelectedStrategy(WebElement _element){
        return (WebDriver d) -> _element.isSelected();
    }

    /**
     * Waits for the given Element to be visible
     *
     * @param _element WebElement
     * @return True if element is enabled, false otherwise
     */
    public static Function<WebDriver, Boolean> waitForVisibleStrategy(WebElement _element){
        return (WebDriver d) -> _element.isDisplayed();
    }

    /**
     * Waits for the given Element to be visible
     *
     * @param _time Integer value representing SECONDS of time to wait
     * @return True if element is enabled, false otherwise
     */
    public static Function<WebDriver, Boolean> waitForTimeStrategy(int _time){
        long _currentTime = System.currentTimeMillis();
        return (WebDriver d) -> System.currentTimeMillis() > (_currentTime + (_time * 1000));
    }

    /**
     * Waits for the given Element to be visible
     * Allows the specification of a TimeUnit
     *
     * @param _value Value representing SECONDS of time to wait
     * @param _unit  TimeUnit format
     * @return True if element is enabled, false otherwise
     */
    public static Function<WebDriver, Boolean> waitForTimeStrategy(int _value, TimeUnit _unit){
        long _currentTime = System.currentTimeMillis();
        return (WebDriver d) -> System.currentTimeMillis() > (_currentTime + (_unit.toMillis(_value)* 1000));
    }
}

package com.qaconsultants.keywords.navigation;

import com.qaconsultants.keywords.Keyword;
import com.qaconsultants.keywords.StandardKeyword;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;


/**
 *
 * Concrete implementation of Keyword responsible for navigating to a URL,
 * forwards, and backwards.
 *
 */
public class Navigate extends StandardKeyword<WebDriver> {
    public enum Destination {
        URL, BACK, FORWARD
    }

    // Navigation Strategy
    private Destination mDestination;

    /**
     * Constructor
     *
     * @param _strategy Enum value determining how to navigate
     */
    public Navigate(Destination _strategy) {
        mDestination = _strategy;
    }

    /**
     * Alternative Constructor
     * <p>
     * Uses a String to determine what Destination enum object to use
     *
     * @param _strategy String value to locate a matching enum with
     */
    public Navigate(String _strategy) {
        mDestination = Destination.valueOf(_strategy);
    }

    @Override
    public boolean Execute(WebDriver _driver, String... _arguments) throws Exception {
        try {
            switch (mDestination) {
                case URL:
                    NavigateToURL(_driver, _arguments[0]);
                    break;
                case BACK:
                    NavigateBack(_driver);
                    break;
                case FORWARD:
                    NavigateForward(_driver);
                    break;
                default:
                    return false;
            }
            return true;
        }
        catch (Exception e) {
            mLogger.error("An error has occurred in the Navigate keyword", e);
            throw e;
        }
    }

    /**
     * Navigate to the supplied URL.
     * Performs a basic URL validity check prior to navigation.
     *
     * @param _driver WebDriver instance
     * @param _url    URL to navigate to
     */
    private void NavigateToURL(WebDriver _driver, String _url) throws Exception {
        if (_url.startsWith("http") || _url.startsWith("file"))
            _driver.navigate().to(_url);
        else
            throw new Exception("Invalid URL");
    }

    /**
     * Navigate to the previous page, if possible.
     *
     * @param _driver WebDriver instance
     */
    private void NavigateBack(WebDriver _driver) {
        _driver.navigate().back();
    }

    /**
     * Navigate to the next page, if possible.
     * This function should only be used if you've previously navigated backwards.
     *
     * @param _driver WebDriver instance
     */
    private void NavigateForward(WebDriver _driver) {
        _driver.navigate().forward();
    }
}

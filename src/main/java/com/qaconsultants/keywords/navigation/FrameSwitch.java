package com.qaconsultants.keywords.navigation;

import com.qaconsultants.keywords.StandardKeyword;
import com.qaconsultants.utils.StringUtils;
import org.openqa.selenium.WebDriver;

/**
 *
 * Concrete implementation of Keyword responsible for switching IFrames on the current page.
 *
 * If the given argument is an integer, it will be parsed and frames will be switched based on index.
 * If the given argument is a string, it will be used to locate and switch based on name/id.
 *
 */
public class FrameSwitch extends StandardKeyword<WebDriver> {
    @Override
    public boolean Execute(WebDriver _driver, String... _arguments) {
        try {
            if (StringUtils.isInteger(_arguments[0]))
                _driver.switchTo().frame(Integer.parseInt(_arguments[0]));            // Switch using the index value
            else
                _driver.switchTo().frame(_arguments[0]);                              // Switch using the frame name
            return true;
        }
        catch (Exception e) {
            mLogger.error("An error has occurred in the FrameSwitch keyword", e);
            throw e;
        }
    }
}

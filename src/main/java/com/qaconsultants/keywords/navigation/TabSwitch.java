package com.qaconsultants.keywords.navigation;

import com.qaconsultants.keywords.StandardKeyword;
import com.qaconsultants.utils.StringUtils;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

/**
 *
 * Concrete implementation of Keyword responsible for switching tabs in the current window.
 *
 * If the given argument is an integer, it will be parsed and tabs will be switched based on index.
 * If the given argument is a string, it will be used to switch based on the title.
 *
 */
public class TabSwitch extends StandardKeyword<WebDriver> {

    @Override
    public boolean Execute(WebDriver _driver, String... _arguments) {
        try {
            ArrayList<String> _tabs = new ArrayList<>(_driver.getWindowHandles());
            if (StringUtils.isInteger(_arguments[0])) {
                _driver.switchTo().window(_tabs.get(Integer.parseInt(_arguments[0]) - 1));
            } else {
                _driver.switchTo().window(_arguments[0]);
            }
            return true;
        }
        catch (Exception e) {
            mLogger.error("An error has occurred in the TabSwitch keyword", e);
            throw e;
        }
    }

}

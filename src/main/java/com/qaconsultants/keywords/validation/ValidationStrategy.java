package com.qaconsultants.keywords.validation;

import org.openqa.selenium.WebElement;
import java.util.function.Function;

public interface ValidationStrategy extends Function<WebElement, Boolean> {
}

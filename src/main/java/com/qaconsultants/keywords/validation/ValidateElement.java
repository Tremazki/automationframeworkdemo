package com.qaconsultants.keywords.validation;

import com.qaconsultants.keywords.Keyword;
import com.qaconsultants.keywords.StandardKeyword;
import com.qaconsultants.keywords.misc.Wait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.function.Function;
import java.util.function.Predicate;


/**
 *
 * Concrete implementation of Keyword responsible for validating various aspects of a WebElement
 * Utilizes Predicates for inline extensibility and lambda support.
 *
 */
public class ValidateElement extends Keyword<ValidationStrategy, WebElement> {

    Wait mWait;

    public ValidateElement() {
    }

    public ValidateElement(Wait _wait) {
    }

    @Override
    public boolean Execute(ValidationStrategy _strategy, WebElement _ele) throws Exception {
        try {
            if(mWait == null) {
                return _strategy.apply(_ele);
            } else {
                return mWait.Execute((driver) -> (_strategy.apply(_ele)));
            }
        }
        catch (Exception e) {
            mLogger.error("An error has occurred in the ValidateElement keyword", e);
            throw e;
        }
    }

    public static ValidationStrategy getTextEqualsStrategy(String _expected){
        return (WebElement e) -> e.getText().equals(_expected);
    }

    public static ValidationStrategy getTextContainsStrategy(String _expected){
        return (WebElement e) -> e.getText().contains(_expected);
    }

    public static ValidationStrategy getAttributeEqualsStrategy(String _attr, String _expected){
        return (WebElement e) -> e.getAttribute(_attr).equals(_expected);
    }

    public static ValidationStrategy getElementEnabledStrategy(boolean _positive){
        return (WebElement e) -> e.isEnabled() == _positive;
    }

    public static ValidationStrategy getElementVisibleStrategy(boolean _positive){
        return (WebElement e) -> e.isDisplayed() == _positive;
    }
}


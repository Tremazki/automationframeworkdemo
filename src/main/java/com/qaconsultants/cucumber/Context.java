package com.qaconsultants.cucumber;

import com.qaconsultants.App;
import com.qaconsultants.config.Config;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import java.io.FileNotFoundException;


/**
 * The context object is responsible for sharing information in a thread between step-definition classes.
 *
 * It is automatically injected into the constructor of an object containing step-definitions via PicoContainer
 * allowing our steps to share state in a thread-safe manner.
 */
public class Context {

    private Logger    mLogger;
    private WebDriver mWebDriver;
    private Config    mConfiguration;

    public Context() throws FileNotFoundException, ParseException {
        mConfiguration = App.setupConfiguration(new String[0]);
        mLogger        = LogManager.getLogger(this.getClass());
        mLogger.info("Creating the context object..");

        // This line of code ensures that we clean up properly if we encounter a SIGINT / SIGTERM
        Runtime.getRuntime().addShutdownHook(new Thread(this::cleanWebDriver));
    }

    public Config getConfiguration () {
        return mConfiguration;
    }

    public void setWebDriver(WebDriver _instance) {
        mWebDriver = _instance;
    }

    public WebDriver getWebDriver() throws NullPointerException {
        if(mWebDriver != null) {
            return mWebDriver;
        } else {
            throw new NullPointerException("WebDriver instance is null in Context object. " +
                    "Ensure the WebDriver has been instantiated prior to attempting to return it.");
        }
    }

    public void cleanWebDriver() {
        if(mWebDriver != null) {
            mWebDriver.quit();
            mWebDriver = null;
        }
    }
}

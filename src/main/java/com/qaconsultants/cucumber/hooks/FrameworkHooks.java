package com.qaconsultants.cucumber.hooks;

import com.qaconsultants.cucumber.Context;
import io.cucumber.java.After;

public class FrameworkHooks {

    private Context mContext;

    public FrameworkHooks(Context _context) {
        mContext = _context;
    }

    @After(order = 1)
    public void after() {
       mContext.cleanWebDriver();
    }
}

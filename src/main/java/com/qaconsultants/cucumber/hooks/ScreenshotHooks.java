package com.qaconsultants.cucumber.hooks;

import com.qaconsultants.cucumber.Context;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class ScreenshotHooks {

    private Context mContext;

    public ScreenshotHooks(Context _context){
        mContext = _context;
    }

    @After(order=2)
    public void after(Scenario scenario) {
       try {
          if(scenario.isFailed()) {
              byte[] screenshot = ((TakesScreenshot) mContext.getWebDriver()).getScreenshotAs(OutputType.BYTES);
              scenario.write("See attached screenshot");
              scenario.embed(screenshot, "image/png", "Screenshot");
          }
       } catch (NullPointerException e) {
           e.printStackTrace();
       }
    }
}

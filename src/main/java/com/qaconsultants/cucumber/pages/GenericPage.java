package com.qaconsultants.cucumber.pages;

import com.qaconsultants.pom.Page;
import org.openqa.selenium.WebDriver;

/**
 * The generic page is a simple object that gives us access to the functionality within the abstract class without
 * specifying a true page. This is more for the ease of hooking up to external libraries ala Cucumber.
 */
public class GenericPage extends Page<GenericPage> {

    public GenericPage(WebDriver _driver) {
        super(_driver);
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getURL() {
        return null;
    }
}

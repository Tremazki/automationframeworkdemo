package com.qaconsultants.misc;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ImmutableCapabilities;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;
import java.util.ArrayList;
import java.net.MalformedURLException;


/**
 *
 * DriverFactory is a helper class created to facilitate the creation of WebDrivers
 *
 */
@SuppressWarnings("unused")
public class DriverFactory {

    /**
     * Browser enumeration that can be used in place of passing unchecked Strings
     */
    public enum Browser {
        FIREFOX, CHROME, IE, GHOST
    }

    private static ArrayList<WebDriver> sCreatedDrivers;
    private static String               sWebDriverRoot;
    private static String               sOS;

    /**
     * Static initializer
     * This will run before any attempt at static access to this class
     */
    static
    {
        sCreatedDrivers = new ArrayList<>();
        sWebDriverRoot  = System.getProperty("qac.webdriverDirectory");
        sOS             = System.getProperty("os.name");

        //Create a shutdown hook that runs when the process is killed with a SIGINT or SIGHUP
        Runtime.getRuntime().addShutdownHook(new Thread(()-> sCreatedDrivers.forEach(WebDriver::quit)));
    }

    /**
     * Set the root directory where all WebDriver executables are stored
     *
     * @param _dir Root directory containing WebDriver executables
     */
    public static void setWebDriverRoot(String _dir) {
        sWebDriverRoot = _dir;
    }

    /**
     * Return a local WebDriver specified by the Browser enum.
     *
     * @param _type Browser enum value
     * @return WebDriver instance
     * @throws Exception If the WebDriver directory is not set or the executables can not be found.
     */
    public static WebDriver getLocalDriver(Browser _type) throws Exception {
        return getLocalDriver(_type, false);
    }

    public static WebDriver getLocalDriver(Browser _type, boolean _headless) throws Exception {
        return getLocalDriver(_type, null, _headless);
    }

    /**
     * Return a local WebDriver specified by the given String representing a Browser enum value
     *
     * @param _type Browser enum value as a String
     * @return WebDriver instance
     * @throws Exception If the WebDriver directory is not set or the executables can not be found.
     */
    public static WebDriver getLocalDriver(String _type) throws Exception {
        return getLocalDriver(_type, false);
    }

    public static WebDriver getLocalDriver(String _type, boolean _headless) throws Exception {
        return getLocalDriver(Browser.valueOf(_type), null, _headless);
    }

    /**
     * Return a local WebDriver specified by the Browser enum.
     * Specify a Capabilities object to pass creation information to the WebDriver
     *
     * @param _type         Browser enum value
     * @param _capabilities Capabilities for this WebDriver to use
     * @return WebDriver instance
     * @throws Exception If the WebDriver directory is not set or the executables can not be found.
     */
    public static WebDriver getLocalDriver(Browser _type, MutableCapabilities _capabilities, boolean _headless) throws Exception {
        if (sWebDriverRoot == null) {
            throw new Exception("Path to the WebDriver executables is empty. Failed to create a browser driver." +
                    "Set the qac.webdriverDirectory system property to an appropriate path or check your configuration file.");
        }

        WebDriver _driver = null;
        switch (_type) {
            case FIREFOX:
                _driver = getFirefoxDriver(_capabilities, _headless);
                break;
            case CHROME:
                _driver = getChromeDriver(_capabilities, _headless);
                break;
            case IE:
                _driver = getIEDriver(_capabilities, _headless);
                break;
        }
        sCreatedDrivers.add(_driver);
        return _driver;
    }

    /**
     * Return a local WebDriver specified by the given String representing a Browser enum value
     * Specify a Capabilities object to pass creation information to the WebDriver
     *
     * @param _type         Browser enum value as a String
     * @param _capabilities Capabilities for this WebDriver to use
     * @return WebDriver instance
     * @throws Exception If the WebDriver directory is not set or the executables can not be found.
     */
    public static WebDriver getLocalDriver(String _type, MutableCapabilities _capabilities, boolean _headless) throws Exception {
        return getLocalDriver(Browser.valueOf(_type.toUpperCase()), _capabilities, _headless);
    }

    /**
     * Return a remote WebDriver instance connecting to the supplied String with the supplied Capabilities
     *
     * @param _caps          Capabilities object used to determine the original state/options of the RemoteWebDriver
     * @param _connectionURL URL that this RemoteWebDriver will attempt to connect to.
     *                       This includes the SAUCE LABS / PERFECTO connection URL or a Grid URL
     * @return Instantiated RemoteWebDriver object
     * @throws MalformedURLException MalformedURLException if the connection URL is an invalid URL
     */
    public static WebDriver getRemoteDriver(MutableCapabilities _caps, String _connectionURL) throws MalformedURLException {
        return new RemoteWebDriver(new URL(_connectionURL), _caps);
    }

    private static FirefoxDriver getFirefoxDriver(MutableCapabilities _capabilities, boolean _headless) {
        System.setProperty("webdriver.gecko.driver", sWebDriverRoot + "geckodriver.exe");
        if(isMac() || isUnix())
            System.setProperty("webdriver.gecko.driver", sWebDriverRoot + "geckodriver");

        FirefoxOptions ffopts = new FirefoxOptions();

        if(_headless)
            ffopts.addArguments("-headless");

        if (_capabilities != null) {
            //Do Firefox Specific Setup Here
        }

        return new FirefoxDriver(ffopts);
    }

    private static ChromeDriver getChromeDriver(MutableCapabilities _capabilities, boolean _headless) {
        System.setProperty("webdriver.chrome.driver", sWebDriverRoot + "chromedriver.exe");
        if(isMac() || isUnix())
            System.setProperty("webdriver.chrome.driver", sWebDriverRoot + "chromedriver");

        ChromeOptions cropts = new ChromeOptions();

        if(_headless)
            cropts.addArguments("--headless");

        if (_capabilities != null) {
            //Do Chrome Specific Setup Here
        }

        return new ChromeDriver(cropts);
    }

    private static InternetExplorerDriver getIEDriver(MutableCapabilities _capabilities, boolean _headless) {
        System.setProperty("webdriver.ie.driver", sWebDriverRoot + "IEDriverServer.exe");
        InternetExplorerOptions ieopts = new InternetExplorerOptions();
        ieopts.enableNativeEvents();
        ieopts.takeFullPageScreenshot();
        ieopts.ignoreZoomSettings();
        if (_capabilities != null) {
            //Do IE Specific Setup Here
        }

        return new InternetExplorerDriver(ieopts);
    }

    /**
     * Creates and returns a remote WebDriver object with the supplied capabilities
     *
     * @param _capabilities Capabilities to pass to remote webdriver
     * @return Remote WebDriver object
     * @throws Exception if driver is unable to instantiate
     */
    private static RemoteWebDriver getRemoteDriver(ImmutableCapabilities _capabilities) throws Exception {
        if (_capabilities != null)
            return new RemoteWebDriver(_capabilities);
        else
            throw new Exception("Invalid Capabilities provided to RemoteWebDriver - Was null");
    }

    /**
     * Check whether or not the OS system property signifies that this is a Windows system
     * @return True if a Windows system
     */
    private static boolean isWindows(){
        return (sOS.contains("win"));
    }

    /**
     * Check whether or not the OS system property signifies that this is an OSX system
     * @return True if a OSX system
     */
    private static boolean isMac(){
        return (sOS.contains("mac"));
    }

    /**
     * Check whether or not the OS system property signifies that this is a Unix system
     * @return True if a Unix system
     */
    private static boolean isUnix(){
        return (sOS.contains("nix") || sOS.contains("nux") || sOS.contains("aix"));
    }
}
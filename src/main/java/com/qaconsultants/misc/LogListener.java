package com.qaconsultants.misc;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


/**
 *
 * LogListener is a concrete implementation of a JUnitRunListener that is responsible for the following:
 *
 * 1) Console output through Log4J ConsoleAppenders
 * 2) File output through Log4J RollingFileAppender + Routing
 * 3) Assigning Threads a new name based on their test context (And thus appropriate folder names for log output)
 * 4) Producing a test summary based on the results of the testing
 *
 */
public class LogListener extends RunListener {
    private Logger  mLogger;
    private int     mSuiteCount;
    private boolean mBreakupLogs;

    public LogListener(Logger _logger, boolean breakupTestLogs) {
        mLogger      = _logger;
        mBreakupLogs = breakupTestLogs;
    }

    //This is some pretty ugly code I'll admit.
    @Override
    public void testRunStarted(Description description) {
        ArrayList<Description> mFirstTier = description.getChildren();
        ArrayList<Description> mSecondTier;
        ArrayList<Description> mThirdTier;
        mSuiteCount = mFirstTier.size();

        for (int i = 0; i < mFirstTier.size(); i++) {
            mSecondTier = mFirstTier.get(i).getChildren();
            mLogger.info(String.format("Gathered Test Suite: [%s] with test classes..", mFirstTier.get(i).getClassName()));
            for (int k = 0; k < mSecondTier.size(); k++) {
                mThirdTier = mSecondTier.get(k).getChildren();
                mLogger.info((k + 1) + ") " + mSecondTier.get(k).getClassName() + " with tests..");
                for (int j = 0; j < mThirdTier.size(); j++) {
                    mLogger.info("\t\t" + (j + 1) + ") " + mThirdTier.get(j).getMethodName());
                }
            }
            mLogger.info("");
        }
    }

    @Override
    public void testRunFinished(Result result) {
        int numberOfTest        = result.getRunCount();
        int numberOfTestFail    = result.getFailureCount();
        int numberOfTestIgnore  = result.getIgnoreCount();
        int numberOfTestSuccess = numberOfTest - numberOfTestFail - numberOfTestIgnore;
        int successPercent      = (numberOfTest != 0) ? numberOfTestSuccess * 100 / numberOfTest : 0;

        mLogger.info("Test results:");
        mLogger.info("----------------------------");
        mLogger.info("Number of suites: \t\t"   + mSuiteCount);
        mLogger.info("Number of tests:  \t\t"   + numberOfTest);
        mLogger.info("Number of passes: \t\t"   + numberOfTestSuccess);
        mLogger.info("Number of fails:  \t\t"   + numberOfTestFail);
        mLogger.info("Number of skips:  \t\t"   + numberOfTestIgnore);
        mLogger.info("Success %:        \t\t"   + successPercent);
        mLogger.info("Run Time:         \t\t"   + TimeUnit.MILLISECONDS.toSeconds(result.getRunTime()) + " seconds");
        mLogger.info("");
        mLogger.info("Errors");
        mLogger.info("----------------------------");

        if (result.getFailureCount() > 0) {
            for (int i = 0; i < result.getFailureCount(); i++) {
                mLogger.error(result.getFailures().get(i).getTestHeader());
            }
        } else {
            mLogger.info("None");
        }
    }

    @Override
    public void testStarted(Description description) {
        String currTest = description.getTestClass().getSimpleName();
        if (mBreakupLogs) {
            ThreadContext.put("ThreadName", currTest);
        }
        Thread.currentThread().setName(currTest);
        mLogger.info(String.format("Starting test.. [%s]", description.getMethodName()));
    }

    @Override
    public void testFinished(Description description) {
        mLogger.info(String.format("Test completed: [%s]", description.getMethodName()));
    }

    @Override
    public void testFailure(Failure failure) {
        mLogger.error(String.format("FAIL: [%s]", failure.getDescription().getMethodName()), failure.getException());
    }

    @Override
    public void testIgnored(Description description) {
        mLogger.info(String.format("SKIP: [%s]", description.getMethodName()));
    }
}

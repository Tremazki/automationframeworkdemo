package com.qaconsultants.pom.pages;

import org.openqa.selenium.WebDriver;
import com.qaconsultants.pom.Page;

/**
 * LandingPage is another Page object that is eventually returned by LoginPage
 * This exists solely for the sake of validating the next page in our example test.
 */
public class LandingPage extends Page<LandingPage> {
    public LandingPage(WebDriver _driver) throws Exception {
        super(_driver);
    }

    @Override
    public String getTitle() {
        return "My Account";
    }

    @Override
    public String getURL() {
        return null;
    }

}

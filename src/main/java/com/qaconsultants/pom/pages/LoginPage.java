package com.qaconsultants.pom.pages;

import com.qaconsultants.pom.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 *
 * LoginPage is a concrete implementation of the abstract Page object.
 *
 * Note that it extends Page<T> where <T> is the new Page.
 * The reason for this is to allow for chaining base methods in subclasses while returning the appropriate Page type.
 *
 */
public class LoginPage extends Page<LoginPage> {
    /**
     * The FindBy annotation is provided by the Selenium library and is used in conjunction with their PageFactory.
     * When this Page is instantiated, it is automatically run through the Selenium PageFactory using the current
     * state of the WebDriver object.
     * <p>
     * We can specify any type of identifier type here: id, xpath, name, and more.
     */
    @FindBy(id = "identifierId")
    public WebElement usernameField;

    @FindBy(id = "identifierNext")
    public WebElement usernameSubmit;

    @FindBy(name = "password")
    public WebElement passwordField;

    @FindBy(id = "passwordNext")
    public WebElement passwordSubmit;


    /**
     * Constructor
     * <p>
     * Ensure to call the super() constructor to allow the base functionality to run this page
     * through the Selenium PageFactory
     *
     * @param _driver WebDriver object
     */
    public LoginPage(WebDriver _driver) {
        super(_driver);
    }

    /**
     * Return the title of this Page.
     * This is used by the base method: validateTitle()
     *
     * @return valid title of this page
     */
    public String getTitle() {
        return "Sign in - Google Accounts";
    }

    /**
     * Return the title of this Page.
     * This is used by the base method: navigate()
     *
     * @return valid url to this page
     */
    public String getURL() {
        return "https://accounts.google.com/signin/v2/identifier?flowName=GlifWebSignIn&flowEntry=ServiceLogin";
    }

    /**
     * Helper method that uses a conjunction of our Page's Selenium actions to set a username and submit it.
     *
     * @param _username Username value
     * @return This Page
     */
    public LoginPage setUsername(String _username) throws Exception {
        inputText(usernameField, _username);
        clickElement(usernameSubmit);
        waitForSeconds(2);
        return this;
    }

    /**
     * Helper method that uses a conjunction of our Page's Selenium actions to set a password and submit it.
     *
     * @param _password Password value
     * @return This Page
     */
    public LoginPage setPassword(String _password) throws Exception {
        inputText(passwordField, _password);
        waitForSeconds(2);
        return this;
    }

    /**
     * Helper method that uses a conjunction of our Page's Selenium actions to wait for the Password submit button
     * to be visible and then clicking it.
     *
     * @return The resulting page - Landing Page
     */
    public LandingPage submit() throws Exception {
        clickElement(passwordSubmit);
        return new LandingPage(getDriver());
    }

    /**
     * Helper method that uses the above helper functions in conjunction to even further simplify the test.
     *
     * @param _username Username value
     * @param _password Password value
     *
     * @return The resulting page - Landing Page
     */
    public LandingPage login(String _username, String _password) throws Exception {
        setUsername(_username);
        setPassword(_password);
        return submit();
    }
}
package com.qaconsultants.pom.pages.automationpractice;

import com.qaconsultants.pom.Page;
import com.qaconsultants.utils.WebUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class SignUp extends Page<SignUp> {

    public enum GENDER { MR, MRS }

    @FindBy(id = "account-creation_form")
    public WebElement signUpForm;

    @FindBy(id = "id_gender1")
    public WebElement mrRadioButton;

    @FindBy(id = "id_gender2")
    public WebElement mrsRadioButton;

    @FindBy(id = "customer_firstname")
    public WebElement pFirstNameInput;

    @FindBy(id = "customer_lastname")
    public WebElement pLastNameInput;

    @FindBy(id = "passwd")
    public WebElement passwordInput;

    @FindBy(id = "days")
    public WebElement dobDayDropdown;

    @FindBy(id = "months")
    public WebElement dobMonthDropdown;

    @FindBy(id = "years")
    public WebElement dobYearDropdown;

    @FindBy(id = "newsletter")
    public WebElement newsLetterCheckbox;

    @FindBy(id = "optin")
    public WebElement specialOffersCheckbox;

    @FindBy(id = "firstname")
    public WebElement aFirstNameInput;

    @FindBy(id = "lastname")
    public WebElement aLastNameInput;

    @FindBy(id = "company")
    public WebElement companyInput;

    @FindBy(id = "address1")
    public WebElement addressLine1Input;

    @FindBy(id = "address2")
    public WebElement addressLine2Input;

    @FindBy(id = "city")
    public WebElement cityInput;

    @FindBy(id = "id_state")
    public WebElement stateDropdown;

    @FindBy(id = "postcode")
    public WebElement postalCodeInput;

    @FindBy(id = "id_country")
    public WebElement countryDropdown;

    @FindBy(id = "other")
    public WebElement otherInformationTextArea;

    @FindBy(id = "phone")
    public WebElement homePhoneInput;

    @FindBy(id = "phone_mobile")
    public WebElement mobilePhoneInput;

    @FindBy(id = "alias")
    public WebElement addressAliasInput;

    @FindBy(id = "submitAccount")
    public WebElement registerButton;

    public SignUp(WebDriver _driver) {
        super(_driver);
    }

    @Override
    public String getTitle() {
        return "Login - My Store";
    }

    @Override
    public String getURL() {
        return "http://automationpractice.com/index.php?controller=authentication&back=my-account#account-creation";
    }

    public void enterPersonalInformation(GENDER _gender,
                                         String _firstName,
                                         String _lastName,
                                         String _password,
                                         int    _dobDay,
                                         int    _dobMonth,
                                         int    _dobYear,
                                         boolean _newsLetter,
                                         boolean _specialOffers) throws Exception {
        // Gender Radio Buttons
        if(_gender.equals(GENDER.MR)) {
            clickElement(mrRadioButton);
        } else {
            clickElement(mrsRadioButton);
        }

        inputText(pFirstNameInput, _firstName);
        inputText(pLastNameInput,  _lastName);
        inputText(passwordInput,   _password);
        selectFromDropdownByValue(dobDayDropdown,   String.valueOf(_dobDay));
        selectFromDropdownByValue(dobMonthDropdown, String.valueOf(_dobMonth));
        selectFromDropdownByValue(dobYearDropdown,  String.valueOf(_dobYear));

        if(_newsLetter) {
            clickElement(newsLetterCheckbox);
        }

        if(_specialOffers) {
            clickElement(specialOffersCheckbox);
        }
    }

    public void enterAddressInformation(String _firstName,
                                        String _lastName,
                                        String _company,
                                        String _addrLine1,
                                        String _addrLine2,
                                        String _city,
                                        String _state,
                                        String _postalCode,
                                        String _country,
                                        String _homePhone,
                                        String _mobilePhone,
                                        String _alias,
                                        String _additInfo) throws Exception {
        inputText(aFirstNameInput  , _firstName);
        inputText(aLastNameInput   , _lastName);
        inputText(companyInput     , _company);
        inputText(addressLine1Input, _addrLine1);
        inputText(addressLine2Input, _addrLine2);
        inputText(cityInput        , _city);
        inputText(postalCodeInput  , _postalCode);

        selectFromDropdownByText(countryDropdown, _country);
        selectFromDropdownByText(stateDropdown  , _state);

        inputText(otherInformationTextArea, _additInfo);
        inputText(homePhoneInput          , _homePhone);
        inputText(mobilePhoneInput        , _mobilePhone);
        inputText(addressAliasInput       , _alias);
    }

    public MyAccount submitApplication() throws Exception {
        clickElement(registerButton);
        return new MyAccount(mWebDriver);
    }

    WebUtils mWebUtils    = new WebUtils(mWebDriver);
    String   errorMessage = "//div[contains(@class,\"alert\") and not(contains(@style, \"display:none\"))]//li";
    public void validateError(String _message) throws Exception {
        List<WebElement> elements = mWebUtils.findElements(By.xpath(errorMessage));
        for(WebElement element : elements) {
           validateVisible(element);
           try {
               validateText(element, _message);
               return;
           } catch (Exception e) {
                continue;
           }
        }
        throw new Exception("Failed to find any error message");
    }
}

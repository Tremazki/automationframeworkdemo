package com.qaconsultants.pom.pages.automationpractice;

import com.qaconsultants.pom.Page;
import org.openqa.selenium.WebDriver;

public class MyAccount extends Page<MyAccount> {

    public MyAccount(WebDriver _driver) {
        super(_driver);
    }

    @Override
    public String getTitle() {
        return "My account - My Store";
    }

    @Override
    public String getURL() {
        return "https://automationpractice.com/index.php?controller=my-account";
    }
}

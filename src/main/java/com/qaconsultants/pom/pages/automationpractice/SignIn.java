package com.qaconsultants.pom.pages.automationpractice;

import com.qaconsultants.pom.Page;
import com.qaconsultants.utils.WebUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignIn extends Page<SignIn> {

    @FindBy(id = "email_create")
    public WebElement emailAddressNew;

    @FindBy(id = "SubmitCreate")
    public WebElement emailSubmitNew;

    @FindBy(id = "email")
    public WebElement emailAddressSignIn;

    @FindBy(id = "passwd")
    public WebElement emailPasswordSignIn;

    @FindBy(id = "SubmitLogin")
    public WebElement emailSubmitSignIn;

    public SignIn(WebDriver _driver) {
        super(_driver);
    }

    @Override
    public String getTitle() {
        return "Login - My Store";
    }

    @Override
    public String getURL() {
        return "http://automationpractice.com/index.php?controller=authentication&back=my-account#account-creation";
    }

    WebUtils mWebUtils    = new WebUtils(mWebDriver);
    String   errorMessage = "//div[contains(@class,\"alert\") and not(contains(@style, \"display:none\"))]//li";
    public void validateError(String _message) throws Exception {
        validateVisible(mWebUtils.findElement(By.xpath(errorMessage)));
        validateText(mWebUtils.findElement(By.xpath(errorMessage)), _message);
    }

    public SignUp createNewEmail(String _email) throws Exception {
        inputText(emailAddressNew, _email);
        clickElement(emailSubmitNew);
        return new SignUp(mWebDriver);
    }

    public MyAccount signInEmail(String _email, String _password) throws Exception {
        inputText(emailAddressSignIn,  _email);
        inputText(emailPasswordSignIn, _password);
        clickElement(emailSubmitSignIn);
        return new MyAccount(mWebDriver);
    }
}

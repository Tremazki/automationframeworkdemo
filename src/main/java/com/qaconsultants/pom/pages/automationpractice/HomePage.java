package com.qaconsultants.pom.pages.automationpractice;

import com.qaconsultants.pom.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends Page<HomePage> {

    @FindBy(xpath = ".//nav/div/a[@class=\"login\"]")
    public WebElement signInButton;

    public HomePage(WebDriver _driver) {
        super(_driver);
    }

    @Override
    public String getTitle() {
        return "My Store";
    }

    @Override
    public String getURL() {
        return "http://automationpractice.com/index.php";
    }

    public SignIn clickSignIn() throws Exception {
        clickElement(signInButton);
        return new SignIn(mWebDriver);
    }
}

package com.qaconsultants.pom;

import com.qaconsultants.keywords.interaction.Click;
import com.qaconsultants.keywords.interaction.Dropdown;
import com.qaconsultants.keywords.interaction.Input;
import com.qaconsultants.keywords.misc.Wait;
import com.qaconsultants.keywords.navigation.FrameSwitch;
import com.qaconsultants.keywords.navigation.Navigate;
import com.qaconsultants.keywords.navigation.TabSwitch;
import com.qaconsultants.keywords.validation.ValidateElement;
import com.qaconsultants.keywords.validation.ValidationStrategy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 *
 * Abstract Class representing all 'Page' objects in a Page Object Model (POM)
 *
 * This abstract class takes care of ensuring that we're on the correct page initially and provides
 * helper functions to all Page objects.
 *
 * The Generic type <T> is used to allow method-chaining of default functionality for subclasses.
 * A new Page object: 'MyPage' that extends Page<MyPage> will be able to return itself from common functions.
 *
 */
@SuppressWarnings({"unchecked", "unused"})
public abstract class Page<T extends Page> {
    //Internal logger for each Page
    protected final Logger mLogger;
    //Internal WebDriver for each Page
    protected WebDriver mWebDriver;

    /**
     * Constructor
     *
     * @param _driver WebDriver instance
     */
    public Page(WebDriver _driver) {
        // Set the internal instance of the WebDriver
        mWebDriver = _driver;
        // Configure the WebDriver to ensure that it is appropriately setup before testing
        // TODO: Set implicit timeout via external configuration - Communicate this to here using a system property
        mWebDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // Initialize the page through Selenium's PageFactory to resolve annotated fields, if any
        PageFactory.initElements(_driver, this);
        //Instantiate a logger based on each subclass
        mLogger = LogManager.getLogger(this.getClass());
    }

    /**
     * Returns the title of this page.
     * <p>
     * This is used in the initial verification of the WebDriver state
     *
     * @return Title of the current page
     */
    public abstract String getTitle();

    /**
     * Returns the URL associated with this page.
     * <p>
     * This is used if the driver doesn't currently have a web-page when being passed to the concrete Page object
     *
     * @return URL of the current page
     */
    public abstract String getURL();

    /**
     * Returns the internal instance of the WebDriver
     *
     * @return Concrete WebDriver object
     */
    public WebDriver getDriver() {
        return mWebDriver;
    }

    /**
     * Waits for the WebElement to be visible
     *
     * @param _element WebElement to wait for
     * @return Page object
     */
    public T waitForVisible(WebElement _element) throws Exception {
        new Wait(mWebDriver).Execute(Wait.waitForVisibleStrategy(_element));
        return (T) this;
    }

    /**
     * Waits for the WebElement to be enabled
     *
     * @param _element WebElement to wait for
     * @return Page object
     */
    public T waitForEnabled(WebElement _element) throws Exception {
        new Wait(mWebDriver).Execute(Wait.waitForEnabledStrategy(_element));
        return (T) this;
    }

    /**
     * Waits for the WebElement to be selected
     *
     * @param _element WebElement to wait for
     * @return Page object
     */
    public T waitForSelected(WebElement _element) throws Exception {
        new Wait(mWebDriver).Execute(Wait.waitForSelectedStrategy(_element));
        return (T) this;
    }

    /**
     * Waits explicitly for the specified period of time
     *
     * @param _time Time to wait for
     * @return Page object
     */
    public T waitForSeconds(int _time) throws Exception {
        new Wait(mWebDriver).Execute(Wait.waitForTimeStrategy(_time));
        return (T) this;
    }
    /**
     * Waits for a yet defined Function that returns a Boolean value.
     *
     * @param _func Functional interface
     * @return Page object
     */
    public T waitFor(Function<WebDriver, Boolean> _func) throws Exception {
        new Wait(mWebDriver).Execute(_func);
        return (T) this;
    }

    /**
     * Navigates to the URL returned by the getURL method of this page
     *
     * @return Page object
     */
    public T navigate() throws Exception {
        if (!(new Navigate("URL").Execute(mWebDriver, getURL())))
            throw new Exception("Failed to navigate to the Page's default URL");
        return (T) this;
    }

    /**
     * Navigates to the specified URL
     *
     * @param _url URL to navigate to
     * @return Page object
     */
    public T navigateTo(String _url) throws Exception {
        if (!(new Navigate("URL").Execute(mWebDriver, _url)))
            throw new Exception(String.format("Failed to navigate to the given url: [%s]", _url));
        return (T) this;
    }

    /**
     * Navigates backwards
     *
     * @return Page object
     */
    public T navigateBack() throws Exception {
        if (!(new Navigate("BACK").Execute(mWebDriver)))
            throw new Exception("Failed to navigate backwards");
        return (T) this;
    }

    /**
     * Navigates forward
     *
     * @return Page object
     */
    public T navigateForward() throws Exception {
        if (!(new Navigate("FORWARD").Execute(mWebDriver)))
            throw new Exception("Failed to navigate forwards");
        return (T) this;
    }

    /**
     * Switch tabs to the specified tab
     * <p>
     * If a number is supplied, then a tab is chosen based on index from a list of available tabs in the current browser window
     * If a string is supplied, then a tab is chosen based on the title of the tab itself
     *
     * @return Page object
     */
    public T switchTab(String _tab) throws Exception {
        if (!(new TabSwitch().Execute(mWebDriver, _tab)))
            throw new Exception(String.format("Failed to switch to tab with name/index: [%s]", _tab));
        return (T) this;
    }

    /**
     * Switch frames to the specified frame
     * <p>
     * If a number is supplied, then a frame is chosen based on index from all available frames on the page
     * If a string is supplied, then a frame is chosen based on the id or name of the frame
     *
     * @return Page object
     */
    public T switchFrame(String _frame) throws Exception {
        if (!(new FrameSwitch().Execute(mWebDriver, _frame)))
            throw new Exception(String.format("Failed to switch to frame with name/id/index: [%s]", _frame));
        return (T) this;
    }

    /**
     * Inputs a String into a WebElement using Selenium's built-in sendKeys functionality.
     * Clears the element prior to inputting the text.
     *
     * @param _element WebElement to act on
     * @param _input   Text to send to the WebElement
     * @return Page object
     */
    public T inputText(WebElement _element, String _input) throws Exception {
        if (!(new Input().Execute(_element, _input)))
            throw new Exception(String.format("Failed to input text: [%s] into web-element", _input));
        return (T) this;
    }

    /**
     * Appends a String into a WebElement using Selenium's built-in sendKeys functionality.
     *
     * @param _element WebElement to act on
     * @param _input   Text to send to the WebElement
     * @return Page object
     */
    public T appendText(WebElement _element, String _input) throws Exception {
        if (!(new Input(true).Execute(_element, _input))) {
            throw new Exception(String.format("Failed to input text: [%s] into web-element", _input));
        }
        return (T) this;
    }

    /**
     * Clears the text of the given WebElement using Selenium's built-in clear() functionality
     *
     * @param _element WebElement to act on
     * @return Page object
     */
    public T clearText(WebElement _element) throws Exception {
        try {
            _element.clear();
        } catch(Exception e) {
            throw new Exception("Failed to clear text from web-element");
        }
        return (T) this;
    }

    /**
     * Selects an option from the drop-down menu by its value
     *
     * @param _element WebElement to act on
     * @param _value   The value of the drop-down element to use as a locator
     * @return Page object
     */
    public T selectFromDropdownByValue(WebElement _element, String _value) throws Exception {
        if (!(new Dropdown(Dropdown.SelectBy.VALUE).Execute(_element, _value))) {
            throw new Exception(String.format("Failed to select from drop-down with value: [%s]", _value));
        }
        return (T) this;
    }

    /**
     * Selects an option from the drop-down menu by its value
     *
     * @param _element WebElement to act on
     * @param _value   The value of the drop-down element to use as a locator
     * @return Page object
     */
    public T selectFromDropdownByText(WebElement _element, String _value) throws Exception {
        if (!(new Dropdown(Dropdown.SelectBy.VISIBLE_TEXT).Execute(_element, _value))) {
            throw new Exception(String.format("Failed to select from drop-down with text: [%s]", _value));
        }
        return (T) this;
    }

    /**
     * Selects an option from the drop-down menu by its index
     *
     * @param _element WebElement to act on
     * @param _value   The value of the drop-down element to use as a locator
     * @return Page object
     */
    public T selectFromDropdownByIndex(WebElement _element, String _value) throws Exception {
        if (!(new Dropdown(Dropdown.SelectBy.INDEX).Execute(_element, _value))) {
            throw new Exception(String.format("Failed to select from drop-down with index: [%s]", _value));
        }
        return (T) this;
    }

    public T selectFromDropdownByIndex(WebElement _element, int _value) throws Exception {
        if (!(new Dropdown(Dropdown.SelectBy.INDEX).Execute(_element, _value))) {
            throw new Exception(String.format("Failed to select from drop-down with index: [%d]", _value));
        }
        return (T) this;
    }

    /**
     * Clicks the given WebElement
     *
     * @param _element WebElement to act on
     * @return Page object
     */
    public T clickElement(WebElement _element) throws Exception {
        if (!(new Click().Execute(_element))) {
            throw new Exception("Failed to click web-element");
        }
        return (T) this;
    }

    /**
     * Validates the title of this page and ensures that the name is as we expect.
     * Uses the concrete class' 'getTitle' method to perform the check.
     *
     * @return Page object
     * @throws Exception Exception if validation fails
     */
    public T validateTitle() throws Exception {
        return validateTitle(getTitle());
    }


    /**
     * Validates the title of this page and ensures that the name is as we expect.
     *
     * @param _title Title of the WebPage
     * @return Page object
     * @throws Exception Exception if validation fails
     */
    public T validateTitle(String _title) throws Exception {
        validateCurrentPage(mWebDriver, _title);
        return (T) this;
    }

    /**
     * Validate the text of the given WebElement against the supplied text
     *
     * @param _element WebElement to act on
     * @param _text    Text to compare
     * @return Page object
     */
    public T validateText(WebElement _element, String _text) throws Exception {
        if (!(new ValidateElement().Execute(ValidateElement.getTextEqualsStrategy(_text), _element))) {
            throw new Exception(String.format("Failed to validate the web-element text is: [%s]", _text));
        }
        return (T) this;
    }

    /**
     * Validate that the text of the given WebElement contains the supplied text
     *
     * @param _element WebElement to act on
     * @param _text    Text to find
     * @return Page object
     */
    public T validateTextContains(WebElement _element, String _text) throws Exception {
        if (!(new ValidateElement().Execute(ValidateElement.getTextContainsStrategy(_text), _element))) {
            throw new Exception(String.format("Failed to validate the web-element contains the text: [%s]", _text));
        }
        return (T) this;
    }

    /**
     * Validate the visibility status of the given WebElement
     *
     * @param _element WebElement to act on
     * @return Page object
     */
    public T validateVisible(WebElement _element) throws Exception {
        if (!(new ValidateElement().Execute(ValidateElement.getElementVisibleStrategy(true), _element))) {
            throw new Exception("Failed to validate the visibility of the element");
        }
        return (T) this;
    }

    /**
     * Validate the enabled status of the given WebElement
     *
     * @param _element WebElement to act on
     * @return Page object
     */
    public T validateEnabled(WebElement _element) throws Exception {
        if (!(new ValidateElement().Execute(ValidateElement.getElementEnabledStrategy(true), _element))) {
            throw new Exception("Failed to validate that the element is enabled - element is disabled");
        }
        return (T) this;
    }

    /**
     * Validate the given attribute on the given WebElement
     *
     * @param _element WebElement to act on
     * @param _attr Attribute to find on the WebElement
     * @param _expected Expected value of the element
     * @return Page object
     */
    public T validateAttribute(WebElement _element, String _attr, String _expected) throws Exception{
         if (!(new ValidateElement().Execute(ValidateElement.getAttributeEqualsStrategy(_attr, _expected), _element))) {
             throw new Exception(String.format("Failed to validate that the attribute: [%s] against the expected text: [%s].", _attr, _expected));
         }
        return (T) this;
    }

    /**
     * Validate against the WebElement using a custom predicate
     *
     * @param _pred Predicate
     * @param _element WebElement to act on
     * @return Page object
     */
    public T validateElement(ValidationStrategy _pred, WebElement _element) throws Exception {
        if(!(new ValidateElement().Execute(_pred, _element))) {
            throw new Exception("Failed to validate the element with the given predicate");
        }
        return (T) this;
    }

    /**
     * Validate this page against another Page' Class<?> object
     *
     * @param _page Page class to compare against
     * @return Page object
     */
    public T validatePage(Class<? extends Page> _page) throws Exception {
        if (!_page.equals(this.getClass())) {
            throw new Exception("Failed to validate page");
        }
        return (T) this;
    }

    /**
     * Validates the current state of the WebDriver object to ensure that we're on the correct page
     *
     * @param _driver Concrete WebDriver object
     * @param _title  Page title, supplied by the abstract GetTitle method
     */
    private void validateCurrentPage(WebDriver _driver, String _title) {
        if(!new WebDriverWait(_driver, 10).until(d -> _title.equals(d.getTitle()))) {
            throw new IllegalStateException("Failed to validate that this is the correct page.");
        }
        mLogger.info("");
    }
}

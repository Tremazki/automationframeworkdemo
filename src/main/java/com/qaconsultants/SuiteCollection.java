package com.qaconsultants;

import com.googlecode.junittoolbox.ParallelSuite;
import org.junit.runners.Suite.SuiteClasses;
import org.junit.runner.RunWith;

/**
 * Run the suites within this suite collection in Parallel using a ParallelSuite.class
 * Otherwise, we can use the default Suite.class to run them sequentially.
 */
@RunWith(ParallelSuite.class)

/**
 * Defines all of the classes within this suite collection.
 * This is where we append additional suites.
 */
@SuiteClasses({
})
public class SuiteCollection {
    /*
     * This area is intentionally left blank.
     * The annotations will take care of all of the heavy lifting.
     */
}

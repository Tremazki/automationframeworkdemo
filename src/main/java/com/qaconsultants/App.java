package com.qaconsultants;

import com.qaconsultants.config.Config;
import com.qaconsultants.config.TOMLParser;
import com.qaconsultants.misc.LogListener;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.util.FileUtils;
import org.junit.runner.Result;
import org.junit.runner.JUnitCore;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * java-generic-framework entry point
 *
 * The Java generic framework was created to facilitate the creation of boiler-plate code that may be common to
 * projects among QAC automation programmers/scripters.
 *
 * The POM.xml has been configured to execute this application in the following manners:
 *  1) Execute through command-line using [mvn exec:java]
 *  2) Compile through command-line using [mvn compile]
 *  2) Execute through the IDE using App.java as an entry point
 *
 * Available Features/Helpers include:
 *  1) A configuration class as well as a custom TOML parser for it is available (See Config.java)
 *  2) A skeleton Command-Line Interface is available in CLI.java
 *  3) A driver factory is available to facilitate the creation of WebDrivers (See DriverFactory.java)
 *  4) A collection of Keyword wrappers for Selenium functionality are available (See com.qaconsultants.keywords package)
 *          - (Note: This is for use with future additions such as a Keyword executor from an external file such as an Excel file)
 *  5) A custom RunListener for the JUnitCore that will display the current test(s) in the console and output it to an external file, along with
 *     any errors that occur during the execution. (See LogListener.java)
 *
 *
 *  It is not expected or even suggested that you should use each and every piece that comes with this.
 *  Just consider this a toolbox with some common functionality/tools that have been necessary in the past and may prove useful in the future.
 *
 *  The Example test currently set-up in the Main function should prove to be a decent starting point and showcases the use of a Page Object Model pattern.
 */
public class App {

    // JUnitCore responsible for running our test suites / test cases
    static JUnitCore sTestRunner        = null;
    static String    sDefaultConfigPath = "./resources/configuration/Config.toml";

    /**
     *
     * Entry point for the application.
     * Performs configuration + setup prior to running JUnit through the JUnitCore
     * This is only for use if you plan on packaging the application into a JAR.
     *
     * Alternatively, if you choose to use your IDE as your test-runner, you are still able to leverage the available Page Object Model or Keywords.
     * Certain set-up may be required for the usage of DriverFactory + Config prior to running-tests through the IDE.
     *
     */
    public static void main(String[] args) throws Exception {
        setupConfiguration(args);

        /*
         * Instantiate a JUnitCore object
         */
        sTestRunner = new JUnitCore();

        /*
         * Add a new LogListener to the JUnitCore to allow for run-time information to be logged using our custom
         * LogListener.java class.
         */
        FileUtils.mkdir(new File(System.getProperty("qac.logDirectory")), true);
        sTestRunner.addListener(new LogListener(LogManager.getLogger("LogListener"), true));

        /*
         * Load the JUnit Tests/Suites into the JUnitCore via an array of unknown class types.
         * TODO: Append your new test/suites here
         */

        Class<?>[] _suiteCollections = {
                SuiteCollection.class
        };


        /*
         * Execute the tests and return a Result object.
         * This Result object contains information pertaining to the executed test such as the number of tests failed.
         *
         * Optional: A ParallelComputer can be added to the JUnitCore as the first parameter to run SUITES in parallel.
         */
        Result result = sTestRunner.run(_suiteCollections);
    }

    /**
     *
     */
    public static Config setupConfiguration(String[] args) throws ParseException, FileNotFoundException {
        Config configuration;
        String configurationPath;

        /*
         * Parse arguments and perform necessary functionality defined in the CLI class
         * Implement your own CLI flags + functionality in the CLI.java class
         */
        CLI cli = new CLI();
        cli.parseArgs(args);

        // If we got a path from the CLI parsing, use it. Otherwise we use the default.
        configurationPath = cli.getConfigPath() != null ? cli.getConfigPath() : sDefaultConfigPath;

        /*
         * Load in an external configuration file using our custom TOML parser
         * Internally, this reads the TOML file and assigns the values to a predefined Java class.
         *
         * The Config class MUST be loaded with a parser prior to attempting to return an instance of Config.
         * An exception is thrown otherwise.
         */
        configuration = new Config(new TOMLParser(), new File(configurationPath));
        setSystemInformation(configuration);

        return configuration;
    }

    /**
     * Use config information to set run-time information such as system properties
     *
     * @param _configuration Config object
     */
    public static void setSystemInformation(Config _configuration) {
        // Setting the ThreadName in the current Thread context will create and organize logs into thread-respective folders.
        // ThreadContext.put("ThreadName", _configuration.project.name);

        // Set the number of threads available by the Parallel runners - Otherwise calculated.
        // Note: This single pool is utilized by ParallelSuite.class + ParallelRunner.class
        // System.setProperty("maxParallelTestThreads", "6");

        // Retrieve the current time and set system properties of this applications starting run date/time
        Calendar cal       = Calendar.getInstance();
        String   startDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
        String   startTime = new SimpleDateFormat("HH-mm-ss"  ).format(cal.getTime());

        System.setProperty("qac.startDate", startDate);
        System.setProperty("qac.startTime", startTime);

        // Set the system properties related to QAC output directories.
        String _outputDir =  startDate+ "/" + startTime + "/";
        System.setProperty("qac.logDirectory",    _configuration.pathing.output + _outputDir + "logs/");
        System.setProperty("qac.reportDirectory", _configuration.pathing.output + _outputDir + "reports/");
        System.setProperty("qac.xmlDirectory",    _configuration.pathing.output + _outputDir + "xml/");

        // Set the qac.webdriverDirectory property for the DriverFactory
        System.setProperty("qac.webdriverDirectory", _configuration.pathing.webdrivers);
    }
}

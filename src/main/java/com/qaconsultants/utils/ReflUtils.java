package com.qaconsultants.utils;

import java.lang.reflect.Field;
import java.util.Arrays;

/**
 *
 * A collection of static methods to be used with/for common Reflection operations that extend beyond the
 * scope of a single class/hierarchy.
 *
 */
public class ReflUtils {

    /**
     * Returns whether or not this class contains the field supplied as a method parameter
     *
     * @param object    The object to locate this field in
     * @param fieldName The name of the field to find
     * @return True if found, False otherwise
     */
    public static boolean doesObjectContainField(Object object, String fieldName) {
        return Arrays.stream(object.getClass().getFields()).anyMatch(f -> f.getName().equals(fieldName));
    }

    /**
     * Returns whether or not this class contains the field supplied as a method parameter
     *
     * @param object    The object to locate this field in
     * @param fieldName The name of the field to find
     * @return True if found, False otherwise
     */
    public static Field getObjectField(Object object, String fieldName) {
        return Arrays.stream(object.getClass().getFields()).filter(f -> f.getName().equals(fieldName)).findFirst().get();
    }

}

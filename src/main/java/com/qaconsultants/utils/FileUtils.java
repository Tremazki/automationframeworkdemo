package com.qaconsultants.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class FileUtils {

    private static String propertyDirectoryPath = "resources/data/properties/";

    public static String readValueFromPropertyFile(String _fileName, String _key) throws IOException, Exception {
        String path = String.format("%s%s%s", propertyDirectoryPath, _fileName, ".properties");

        Properties properties = new Properties();
        properties.load(new FileInputStream(path));

        if(properties.containsKey(_key)) {
            return properties.getProperty(_key);
        } else {
           throw new Exception(String.format("Failed to locate the given key: [%s] in the properties file: [%s]",
                   _key,
                   _fileName)
           );
        }
    }
}

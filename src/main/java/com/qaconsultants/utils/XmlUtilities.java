package com.qaconsultants.utils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class XmlUtilities {

    public static <T> void marshalXML(T _type, String _location) throws JAXBException {
        JAXBContext context    = JAXBContext.newInstance(_type.getClass());
        Marshaller  marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(_type, new File(_location));
    }

    public static <T> T unmarshalXML(T _type, String _location) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(_type.getClass());
        Unmarshaller unmarshaller = context.createUnmarshaller();
        return (T) unmarshaller.unmarshal(new File(_location));
    }
}

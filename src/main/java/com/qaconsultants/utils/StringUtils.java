package com.qaconsultants.utils;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Random;

/**
 *
 * A collection of static methods to be used with/for common String operations that extend beyond the
 * scope of a single class/hierarchy.
 *
 * Source: https://stackoverflow.com/questions/5439529/determine-if-a-string-is-an-integer-in-java
 */
public class StringUtils {



    public static String generateRandomString(int length) {
        String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
        String CHAR_UPPER = CHAR_LOWER.toUpperCase();
        String NUMBER     = "0123456789";
        String DATA_FOR_RANDOM_STRING = CHAR_LOWER + CHAR_UPPER + NUMBER;

        if (length < 1) {
            throw new IllegalArgumentException();
        }

        Random        random  = new Random();
        StringBuilder builder = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int  rndCharAt = random.nextInt(DATA_FOR_RANDOM_STRING.length());
            char rndChar   = DATA_FOR_RANDOM_STRING.charAt(rndCharAt);
            builder.append(rndChar);

        }
        return builder.toString();
    }

    /**
     * Helper function for calling the isInteger implementation with a base-10 radix
     *
     * @param s String representation of an integer
     * @return True if the String is a valid integer, False otherwise
     */
    public static boolean isInteger(String s) {
        return isInteger(s, 10);
    }

    /**
     * Examines each character in the supplied String and determines whether or not
     * it is a digit based on the base-10 radix
     *
     * @param s     String representation of an integer
     * @param radix Radix of the supplied integer (Numeric system)
     * @return True if the String is a valid integer, False otherwise
     */
    public static boolean isInteger(String s, int radix) {
        if (s.isEmpty()) {
            return false;
        }
        for (int i = 0; i < s.length(); i++) {
            if (i == 0 && s.charAt(i) == '-') {
                if (s.length() == 1) {
                    return false;
                } else {
                    continue;
                }
            }
            if (Character.digit(s.charAt(i), radix) < 0) {
                return false;
            }
        }
        return true;
    }
}

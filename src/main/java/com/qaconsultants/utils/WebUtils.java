package com.qaconsultants.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class WebUtils {

    WebDriverWait mWait;
    WebDriver     mDriver;

    public WebUtils(WebDriver _driver) {
        mDriver = _driver;
        mWait   = new WebDriverWait(_driver, 10);
    }

    public WebUtils(WebDriver _driver, int waitTimeout) {
        mDriver = _driver;
        mWait   = new WebDriverWait(_driver, waitTimeout);
    }

    public WebElement findElement(By _locator) {
        return mWait.until(ExpectedConditions.presenceOfElementLocated(_locator));
    }

    public List<WebElement> findElements(By _locator) {
        return mWait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(_locator));
    }
}

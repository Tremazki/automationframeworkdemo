package com.qaconsultants.cucumber.stepdefinitions;

import com.qaconsultants.cucumber.Context;
import com.qaconsultants.pom.pages.automationpractice.HomePage;
import com.qaconsultants.pom.pages.automationpractice.MyAccount;
import com.qaconsultants.pom.pages.automationpractice.SignIn;
import com.qaconsultants.pom.pages.automationpractice.SignUp;
import com.qaconsultants.utils.StringUtils;
import com.qaconsultants.utils.WebUtils;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class AutomationPracticeSteps {

    // Pages
    HomePage  mHome;
    SignIn    mSignIn;
    SignUp    mSignUp;
    MyAccount mMyAccount;

    WebUtils mWebUtils;
    Context  mContext;

    public AutomationPracticeSteps(Context _context) {
        mContext  = _context;
        mWebUtils = new WebUtils(mContext.getWebDriver(), _context.getConfiguration().selenium.timeout);
    }

    @When("(the user )navigate(s) to the Automation Practice page")
    public void navigateToHome() throws Exception {
        mHome = new HomePage(mContext.getWebDriver());
        mHome.navigate();
    }

    @When("(the user )click(s) on the sign-in button")
    public void clickSignIn() throws Exception {
        if(mHome == null){
            throw new Exception("User is not currently on the Home page, can not click the sign-in button." +
                    "Try navigating to the home page first.");
        }
        mSignIn = mHome.clickSignIn();
    }

    @When("(the user )logs in using e-mail {string} and password {string}")
    public void loginGivenCredentials(String _username, String _password) throws Exception {
        if(mSignIn == null){
            throw new Exception("User is not currently on the Sign In page, can not begin a new application.");
        }
        mMyAccount = mSignIn.signInEmail(_username, _password);
    }

    @When("(the user )begins a new sign-up application with the e-mail {string}")
    public void beginSignUpApplication(String _email) throws Exception {
        if(mSignIn == null){
            throw new Exception("User is not currently on the Sign In page, can not begin a new application.");
        }
        mSignUp = mSignIn.createNewEmail(_email);
    }

    @When("(the user )begins a new sign-up application with a randomly generated e-mail")
    public void beginSignUpApplicationRandom() throws Exception {
        String email = String.format("%s@%s.com", StringUtils.generateRandomString(10), "qactest");
        beginSignUpApplication(email);
    }

    @When("(the user )fills out the personal information form with valid information")
    public void fillPersonalInformationSignUp() throws Exception {
        if(mSignUp == null) {
            throw new Exception("User is not currently on the Sign Up page, can not fill the application.");
        }
        mSignUp.enterPersonalInformation(SignUp.GENDER.MR,
                "Tyler",
                "Tester",
                "password1234!",
                29,
                11,
                1993,
                false,
                false
        );
    }

    @When("(the user )fills out the address information form with valid information")
    public void fillAddressInformationSignUp() throws Exception {
        if(mSignUp == null) {
            throw new Exception("User is not currently on the Sign Up page, can not fill the application.");
        }
        mSignUp.enterAddressInformation(
                "Tyler",
                "Tester",
                "QAC",
                "100 Fake Street",
                "Apartment 1",
                "Toronto",
                "Alaska",
                "12345",
                "United States",
                "1231231234",
                "1233213214",
                "Default",
                ""
        );
    }

    @When("(the user )submits the sign-up application")
    public void submitApplication() throws Exception {
        if(mSignUp == null) {
            throw new Exception("User is not currently on the Sign Up page, can not submit the application.");
        }
        mMyAccount = mSignUp.submitApplication();
    }

    @Then("(the system )displays sign-in error {string}")
    public void validateSignInError(String _error) throws Exception {
        if(mSignIn == null) {
            throw new Exception("User is not currently on the Sign In page, can not validate displayed error.");
        }
        mSignIn.validateError(_error);
    }

    @Then("(the user )will be on the sign-in page")
    public void validateSignIn() throws Exception {
        mSignIn.validateTitle();
    }

    @Then("(the user )will be on the sign-up page")
    public void validateSignUp() throws Exception {
        mSignUp.validateTitle();
    }

    @Then("(the user )will be on the MyAccount page")
    public void theUserWillBeOnTheMyAccountPage() throws Exception {
        mMyAccount.validateTitle();
    }

    @Then("(the system )will display error {string} in the sign-up form")
    public void theSystemWillDisplayErrorInTheSignUpForm(String error) throws Exception {
        mSignUp.validateError(error);
    }
}

package com.qaconsultants.cucumber.stepdefinitions;

import com.qaconsultants.cucumber.Context;
import com.qaconsultants.cucumber.pages.GenericPage;
import com.qaconsultants.utils.FileUtils;
import com.qaconsultants.utils.WebUtils;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * The GenericStepWrapper is responsible for wrapping keyword functionality in cucumber step-definitions.
 * The keywords are accessed through the GenericPage object.
 */
public class GenericStepWrapper {

    GenericPage mKeywords;
    WebUtils    mWebUtils;

    public GenericStepWrapper(Context _context) {
        WebDriver driver = _context.getWebDriver();

        mKeywords = new GenericPage(driver);
        mWebUtils = new WebUtils(driver, _context.getConfiguration().selenium.timeout);
    }

    //Format: type:source:key
    @ParameterType(name="locator", value="\"(.*?)\"")
    public By locator(String _value) throws Exception {
        String[] parts = _value.split(":");
        String   type    = "";
        String   source  = "";
        String   key     = "";
        String   locator = "";

        if(parts.length != 3){
            throw new Exception("Locator format is incorrect - Expected the following: 'type:source:key'.");
        } else {
            type   = parts[0];
            source = parts[1];
            key    = parts[2];
        }

        switch(type.toLowerCase()) {
            case "prop":
            case "properties":
                locator = FileUtils.readValueFromPropertyFile(source, key);
                break;
            case "excel":
            case "xlsx":
                break;
        }

        return By.xpath(locator);
    }

//    @ParameterType(name="generate", value="\"gen:(.*?)\"")
//    public String generateString(String _value) throws Exception {
//
//    }

    @When("(the user )throw(s) an error intentionally")
    public void throwError() throws Exception {
        throw new Exception("Throwing an intentional exception");
    }

    @When("(the user )wait(s) for {int} second(s)")
    public void waitForSeconds(int seconds) throws Exception {
        mKeywords.waitForSeconds(seconds);
    }

    @When("(the user )navigate(s) to the given URL: {string}")
    public void navigateToURL(String _url) throws Exception {
        mKeywords.navigateTo(_url);
    }

    @When("(the user )inputs the text: {string} into the element: {locator}")
    public void inputTextIntoElement(String _text, By _locator) throws Exception {
        mKeywords.inputText(mWebUtils.findElement(_locator), _text);
    }

    @When("(the user )click(s) the element: {locator}")
    public void clickElement(By _locator) throws Exception {
        mKeywords.clickElement(mWebUtils.findElement(_locator));
    }

    @When("(the user )select(s): {string} by text from the dropdown element: {locator}")
    public void selectFromDropdownText(String _value, By _locator) throws Exception {
        mKeywords.selectFromDropdownByText(mWebUtils.findElement(_locator), _value);
    }

    @When("(the user )select(s): {string} by value from the dropdown element: {locator}")
    public void selectFromDropdownValue(String _value, By _locator) throws Exception {
        mKeywords.selectFromDropdownByText(mWebUtils.findElement(_locator), _value);
    }

    @When("(the user )select(s): {int} by index from the dropdown element: {locator}")
    public void selectFromDropdownIndex(int _index, By _locator) throws Exception {
        mKeywords.selectFromDropdownByIndex(mWebUtils.findElement(_locator), _index);
    }

    @Then("(the user )validate(s) the text of element: {locator} is: {string}")
    public void validateElementText(String _value, By _locator) throws Exception {
        mKeywords.validateText(mWebUtils.findElement(_locator), _value);
    }

    @Then("(the user )validate(s) the element: {locator} is visible")
    public void validateElementVisible(By _locator) throws Exception {
        mKeywords.validateVisible(mWebUtils.findElement(_locator));
    }

    @Then("(the user )validate(s) the element: {locator} is enabled")
    public void validateElementEnabled(By _locator) throws Exception {
        mKeywords.validateEnabled(mWebUtils.findElement(_locator));
    }
}

package com.qaconsultants.cucumber.stepdefinitions;

import com.qaconsultants.cucumber.Context;
import com.qaconsultants.misc.DriverFactory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

public class Preconditions {

    // Shared Context Object
    Context mContext;

    public Preconditions(Context _context) {
        mContext = _context;
    }

    @Given("(the user )open(s) the/a browser( window)")
    public void openDevice() throws Exception {
        //todo externalize the device objects
        mContext.setWebDriver(DriverFactory.getLocalDriver(DriverFactory.Browser.FIREFOX));
    }

    @When("(the user )close(s) the/a browser( window)")
    public void closeDevice() throws Exception {
        mContext.cleanWebDriver();
    }
}

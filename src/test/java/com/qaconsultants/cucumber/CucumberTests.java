package com.qaconsultants.cucumber;

import com.qaconsultants.junit.QACTest;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        glue     = {"com.qaconsultants.cucumber"},
        features = {"src/test/resources/features"},
        plugin   = {"pretty",
                    "html:target/cucumber",
                    "json:target/cucumber.json",
                    "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"},
        tags     = {}
)
public class CucumberTests extends QACTest {
}

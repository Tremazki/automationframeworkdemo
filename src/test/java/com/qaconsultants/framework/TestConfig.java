package com.qaconsultants.framework;

import com.qaconsultants.config.Config;
import com.qaconsultants.config.TOMLParser;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

public class TestConfig  {

    private static final String mConfigLocation = "./resources/configuration/Config.toml";

    @Test
    public void testConfigPropertyAccess() throws Exception {
        Config config = new Config(new TOMLParser(), new File(mConfigLocation));

        assertEquals(config.project.name, "Project Name");
        assertEquals(config.pathing.output, "./output/");
    }

    @Test
    public void testConfigFieldSet() throws Exception {
        Config config = new Config(new TOMLParser(), new File(mConfigLocation));
        assertEquals(config.project.name, "Project Name");

        config.setSectionField("project", "name", "NEW VALUE");
        assertEquals(config.project.name, "NEW VALUE");
    }

    @Test
    public void testTOMLParser() throws Exception {
        Config config = new Config(new TOMLParser(), new File(mConfigLocation));
        assertNotNull(config);
    }

    @Test(expected= FileNotFoundException.class)
    public void testTOMLParserNoFile() throws Exception {
        Config config = new Config(new TOMLParser(), new File("src/test/resources/NotReal.toml"));
    }

    @Test(expected=FileNotFoundException.class)
    public void testTOMLParserDirectory() throws Exception {
        Config config = new Config(new TOMLParser(), new File("src/test/resources/"));
    }

    @Test(expected=NullPointerException.class)
    public void testTOMLParserNull() throws Exception {
        Config config = new Config(new TOMLParser(), null);
    }
}

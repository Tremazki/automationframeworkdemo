Feature: Sign-up for the Automation Practice website

  Background:
    Given the user opens the browser
    And navigates to the Automation Practice page
    And clicks on the sign-in button
    Then the user will be on the sign-in page
    When the user begins a new sign-up application with a randomly generated e-mail
    Then the user will be on the sign-up page

  Scenario: Sign-up with valid user information
    When the user fills out the personal information form with valid information
    And fills out the address information form with valid information
    And submits the sign-up application
    Then the user will be on the MyAccount page
    And the user closes the browser window

  Scenario Outline: Sign-up without filling in required fields and validate error(s)
    When the user submits the sign-up application
    Then the system will display error "<error>" in the sign-up form
    And the user closes the browser window

    Examples:
     | error                                                                            |
     | You must register at least one phone number.                                     |
     | lastname is required.                                                            |
     | passwd is required.                                                              |
     | address1 is required.                                                            |
     | city is required.                                                                |
     | The Zip/Postal code you've entered is invalid. It must follow this format: 00000 |
     | This country requires you to choose a State.                                     |



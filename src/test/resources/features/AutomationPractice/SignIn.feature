Feature: Sign-in to the Automation Practice website

  Background:
    Given the user opens the browser window
    And navigates to the Automation Practice page
    And clicks on the sign-in button
    Then the user will be on the sign-in page

  Scenario Outline: Login with a valid existing user
    When the user logs in using e-mail "<email>" and password "<password>"
    Then the user will be on the MyAccount page
    And the user closes the browser window

    Examples:
      | email                      | password      |
      | tremazki@qaconsultants.com | password1234! |

  Scenario Outline: Invalid login error validation
    When the user logs in using e-mail "<email>" and password "<password>"
    Then the system displays sign-in error "<error>"
    And the user closes the browser window

    Examples:
      | email                          | password      | error                 |
      | unregistered@qaconsultants.com | password1234! | Authentication failed.|
      | unregistered@qaconsultants.com |               | Password is required. |
      | unregistered@.com              |               | Invalid email address.|
      | unregistered@qaconsultants.com | pass          | Invalid password.     |
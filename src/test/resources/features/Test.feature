Feature: Testing Cucumber Functionality

  @Sprint_1
  Scenario: Navigate to Reddit
    Given the user opens a browser window
    And the user navigates to the given URL: "https://www.reddit.com"
    And throws an error intentionally

  @Sprint_1
  Scenario: Navigate to Google
    Given the user opens the browser window
    And navigates to the given URL: "https://www.google.com"
    When the user inputs the text: "Test Automation" into the element: "prop:google:SearchBar"
    And clicks the element: "prop:google:SearchButton"

